// CS1010 AY2011/2 Semester 1 
// PE2 Ex1: food.c
// Name: 
// Matriculation number: 
// plab account-id: 
// Discussion group: 
// Description of this program:

#include <stdio.h>

int enumerate(int);
void best_combination(int, int, int, int *, int *);

int main(void) 
{
	int num_meals; // for part (a)
	// The variables below are for part (b)
	int ff_cost, hf_cost;   // cost of a fast-food (health-food) meal
	int budget;
	int ff_meals, hf_meals; // number of fast-food (health-food) meals

	// Part (a)
	printf("Enter total number of meals: ");
	scanf("%d", &num_meals);
	printf("Number of combinations = %d\n", enumerate(num_meals));

	// Part (b)
	printf("\nEnter budget: $");
	scanf("%d", &budget);
	printf("Enter fast-food cost per meal: $");
	scanf("%d", &ff_cost);
	printf("Enter health-food cost per meal: $");
	scanf("%d", &hf_cost);

	best_combination(budget, ff_cost, hf_cost, &ff_meals, &hf_meals);
	printf("Number of fast-food meals = %d\n", ff_meals);
	printf("Number of health-food meals = %d\n", hf_meals);

	return 0;
}

// To return number of ways to eat n meals, n >= 1
// such that no 2 consecutive meals are fast-food meals.
int enumerate(int n)
{
	if(n == 1){
		return 2;
	}
	else if(n == 2){
		return 3;
	}
	else {
		//n-1 is for if current meal is healthy. n-2 is for if current meal is unhealthy.
		return enumerate(n - 1) + enumerate(n - 2);
	}
}

// Fill in description of this function
void best_combination(int budget, int ff_cost, int hf_cost, 
                      int *ff_num_p, int *hf_num_p) 
{
	int curr_leftover, min_leftover, ff, hf;

	//initialisation, sort of.
	ff = budget/ff_cost;
	min_leftover = ff * ff_cost;
	*ff_num_p = ff;
	*hf_num_p = 0;

	//loop deducts 1 fast food everytime, and calculates the max fast food and max healthy food
	//if the new leftover is less than the old one, the old one is replaced by the new one
	while(ff != 0){
		ff = ff - 1;
		curr_leftover = budget - (ff * ff_cost);
		hf = curr_leftover/hf_cost;
		curr_leftover = curr_leftover - (hf * hf_cost);

		if(curr_leftover <= min_leftover && hf > *hf_num_p){
			min_leftover = curr_leftover;
			*ff_num_p = ff;
			*hf_num_p = hf;
		}
	}
}

