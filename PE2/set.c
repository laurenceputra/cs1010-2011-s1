// CS1010 AY2011/2 Semester 1
// PE2 Ex2: set.c
//
// Name: 
// Matriculation number: 
// plab account-id: 
// Discussion group: 
//
// Description: this program reads two sets from user input
// and calculate maximum value, or set union, or set difference.

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#define MAX 10

// function prototypes
int read_set(int [], int);                     // completed
int set_max(int [], int, int [], int);         // Part B 
int set_union(int [], int, int [], int);       // Part C
int set_difference(int [], int, int [], int);  // Part D
void ordered_print_set(int [], int);           // completed

int main(void) 
{
	int setA[2*MAX], setB[MAX];  // two sets, each contain no more than 10 values
	int sizeA, sizeB;            // acctual number of values in two sets
	int maximum;                 // maximum value in two sets
	char command;   // input command, D for set difference, M for max value, U for set union

	// read two sets, return the actual number of values in each set
	sizeA = read_set(setA, 1);
	sizeB = read_set(setB, 2);

	printf("Enter command (M for max value; U for union; D for difference): ");
	///////////////////////////////////////////////////////////////////////////
	// PART A. (~10%)
	//
	// Read in a user command and invoke respective function to compute
	// either maximum value, or set union, or set difference.
	// Call ordered_print_set function to print out updated setA after that.
	//////////////////////////////////////////////////////////////////////////

	// fill in your code below

	// some prompts provided below to save your time

	//printf("The difference of two sets is (in ascending order): ");
	//printf("Max value is: %d\n", maximum);
	//printf("The union of two sets is (in ascending order): ");

	scanf(" %c", &command);
	switch(command){
		case 'M':
			maximum = set_max(setA, sizeA, setB, sizeB);
			printf("Max value is: %d\n", maximum);
			break;

		case 'U':
			sizeA = set_union(setA, sizeA, setB, sizeB);
			printf("The union of two sets is (in ascending order): ");
			ordered_print_set(setA, sizeA);
			break;

		case 'D':
			sizeA = set_difference(setA, sizeA, setB, sizeB);
			printf("The difference of two sets is (in ascending order): ");
			ordered_print_set(setA, sizeA);
			break;
	}

	return 0;
}


// read elements of a set one by one
// Given, NOT to be changed!
int read_set(int set[], int index)
{
	int i, num;

	printf("Enter the number of elements in set %d: ", index);
	scanf("%d", &num);
	printf("Enter the %d elements: ", num);
	for (i=0; i<num; i++)
		scanf("%d", &set[i]); 

	return num;
}


///////////////////////////////////////////////////////////////////////////
// PART B.  set_max function (~5%)
//
// Find overall maximum value in both setA and setB.
//////////////////////////////////////////////////////////////////////////
int set_max(int setA[], int sizeA, int setB[], int sizeB)
{
	int i, max = setA[0];
	for(i = 1; i < sizeA; i++){
		if(max < setA[i]){
			max = setA[i];
		}
	}
	for(i = 0; i < sizeB; i++){
		if(max < setB[i]){
			max = setB[i];
		}
	}

	return max;  // stub, to be replaced by actual return value
}


///////////////////////////////////////////////////////////////////////////
// PART C.  set_union (~15%)
//
// Update setA with the union of setA and setB.
// Return updated size of setA (i.e., actual number of values in setA).
//////////////////////////////////////////////////////////////////////////
int set_union(int setA[], int sizeA, int setB[], int sizeB)
{
	int i, j, exists;
	for(i = 0; i < sizeB; i++){
		exists = 0;
		for(j = 0; j < sizeA; j++){
			if(setB[i] == setA[j]){
				exists = 1;
			}
		}
		if(exists != 1){
			setA[sizeA] = setB[i];
			sizeA++;
		}
	}

	return sizeA;  // updated size of setA
}


///////////////////////////////////////////////////////////////////////////
// PART D.  set_difference (~20%)
//
// Update setA with the difference of setA and setB.
// Return updated size of setA (i.e., actual number of values in setA).
//////////////////////////////////////////////////////////////////////////
int set_difference(int setA[], int sizeA, int setB[], int sizeB)
{
	int i, j, k, exists;
	for(i = 0; i < sizeB; i++){
		exists = 0;
		for(j = 0; j < sizeA && exists == 0; j++){
			if(setB[i] == setA[j]){
				exists = 1;
			}
		}
		if(exists == 1){
			sizeA--;
			for(k = j - 1; k < sizeA; k++){
				setA[k] = setA[k+1];
			}
		}
		else{
			setA[sizeA] = setB[i];
			sizeA++;
		}
	}

	return sizeA;  // updated size of setA
}


// print a set in ascending order of elements
// Given, NOT to be changed!
void ordered_print_set(int set[], int size)
{
	int i, start_index, min_index, temp;

	if (size == 0)
	{
		printf(" null\n");
		return;
	}

	// selection sort of a set in ascending order of elements
	for (start_index=0; start_index<size-1; start_index++)
	{
		// find the index of minimum element 
		min_index = start_index;
		for (i=start_index+1; i<size; i++) 
			if (set[i] < set[min_index]) 
				min_index = i;

		// swap minimum element with element at start_index
		temp = set[start_index];
		set[start_index] = set[min_index];
		set[min_index] = temp;
	}

	for (i=0; i<size; i++)
		printf(" %d", set[i]);
	printf("\n");
}
