/**
 * CS1010 AY2011/2 Semester 1 Lab5 Ex1
 * palindrome_numbers.c
 * <Type in description of program>
 * <Type your name here>
 * <Type your discussion group here>
 */

#include <stdio.h>

#define MAX_SIZE 9

int findPalindrome(int start, int end);
int checkPalindrome(int num);
int checkPalindromeAux(int num[], int low, int high);

int main(void)
{
	int count;
	int start, end;

	printf("Enter start and end: ");
	scanf("%d %d", &start, &end);

	count = findPalindrome(start, end);
	printf("Number of palindrome numbers = %d\n", count);

	return 0;
}

int findPalindrome(int start, int end){
	int i, count = 0;
	for(i = start; i <= end; i++){
		if(checkPalindrome(i) == 1){
			count++;
		}
	}
	return count;
}
int checkPalindrome(int num){
	int numarr[MAX_SIZE];
	int count = 0;
	while(num != 0){
		numarr[count] = num % 10;
		num = num / 10;
		count++;
	}
	return checkPalindromeAux(numarr, 0, count - 1);
}
int checkPalindromeAux(int num[], int low, int high){
	if(low >= high){
		return 1;
	}
	else if(num[low] == num[high]){
		return checkPalindromeAux(num, low + 1, high - 1);
	}
	else{
		return 0;
	}
	
}