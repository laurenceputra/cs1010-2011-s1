#include <stdio.h>
#include <string.h>

#define MAX_SENTENCE_SIZE 40
#define MAX_EDITED_SIZE 150

void pig_latin(char src[], char dest[]);

int main(){
	char sentence[MAX_SENTENCE_SIZE], edited[MAX_EDITED_SIZE];
	int length;

	printf("Enter sentence: ");
	fgets(sentence, MAX_SENTENCE_SIZE, stdin);
	length = strlen(sentence);
	if(sentence[length - 1] == '\n'){
		sentence[length - 1] = '\0';
	}
	pig_latin(sentence, edited);
	printf("Converted: %s\n", edited);

}

void pig_latin(char src[], char dest[]){
	char *token;
	int counter_src = 0, counter_dest = -1;
	token = strtok(src, " ");
	while(token != NULL){
		counter_dest++;
		if(token[0] == 'a' || token[0] == 'e' || token[0] == 'i' || token[0] == 'o' || token[0] == 'u'){
			counter_src = 0;
			while(token[counter_src] != '\0'){
				dest[counter_dest] = token[counter_src];
				counter_dest++;
				counter_src++;
			}
			dest[counter_dest] = 'w';
			dest[++counter_dest] = 'a';
			dest[++counter_dest] = 'y';
		}
		else {
			counter_src = 1;
			while(token[counter_src] != '\0'){
				dest[counter_dest] = token[counter_src];
				counter_dest++;
				counter_src++;
			}
			dest[counter_dest] = token[0];
			dest[++counter_dest] = 'a';
			dest[++counter_dest] = 'y';
		}
		dest[++counter_dest] = ' ';
		token = strtok(NULL, " ");
	}
	dest[counter_dest] = '\0';
}