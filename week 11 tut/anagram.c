#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define MAX_SIZE 40

void cleans(char *str);
void insertionsort(char *str);
int compare(char *str1, char *str2);

int main(){
	char str1[MAX_SIZE], str2[MAX_SIZE];
	printf("Enter the first string.\n");
	fgets(str1, MAX_SIZE, stdin);
	printf("Enter the second string.\n");
	fgets(str2, MAX_SIZE, stdin);

	cleans(str1);
	cleans(str2);

	insertionsort(str1);
	insertionsort(str2);

	printf("%s\n", str1);
	printf("%s\n", str2);
	printf("The 2 strings are %sanagrams.\n",compare(str1, str2)==1?"":"not ");
	return 0;
}

void cleans(char *str){
	int i, j, len = strlen(str);

	for (i=0, j=0; i<len; i++)
		if (isalpha(str[i]))
			str[j++] = tolower(str[i]);

	str[j] = '\0';
}

void insertionsort(char *str){
	int i, j, temp, len = strlen(str);

	for(i = 1; i < len; i++){
		temp = str[i];
		//printf("%c", temp);
		for(j = i - 1; j >= 0 && str[j] > temp; j--){
			str[j + 1] =  str[j];
		}
		str[j + 1] = temp;
	}
}

int compare(char *str1, char *str2){
	int i, len1 = strlen(str1), len2 = strlen(str2);
	if(len1 != len2){
		return 0;
	}
	for(i = 0; i < len1; i++){
		if(str1[i] != str2[i] && (isalpha(str1[i]) || isalpha(str2[i]))){
			return 0;
		}
	}
	return 1;
}