#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_SIZE 20

void printArray(int arr[]);
void populateArray(int arr[]);
void bubblesort(int arr[]);

int main(){
	int arr[MAX_SIZE];
	populateArray(arr);
	printArray(arr);
	bubblesort(arr);
	printArray(arr);
	return 0;
}

void printArray(int arr[]){
        int i;

        for(i = 0; i < MAX_SIZE; i++){
                printf("%d ", arr[i]);
        }
        printf("\n");
}

void populateArray(int arr[]){
        int i;
        srand(time(NULL));

        for(i = 0; i < MAX_SIZE; i++){
                arr[i] = rand() % 100 - 50;
        }
}

void bubblesort(int arr[]){
	int swap = 1, i, j, num_to_bubble, temp;

	//continues running only if swap == 1, basically if a swap occurs
	for(i = 0; i < MAX_SIZE && swap == 1; i++){
		//resets swap to 0 at the start of every sweep
		swap = 0;
		num_to_bubble = MAX_SIZE - i;
		for(j = 1; j < num_to_bubble; j++){
			if(arr[j - 1] > arr[j]){
				temp = arr[j - 1];
				arr[j - 1] = arr[j];
				arr[j] = temp;
				//changes swap to 1 to record that at least a swap has been made
				swap = 1;
			}
		}
	}
}