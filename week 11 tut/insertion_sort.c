#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_SIZE 20

void printArray(int arr[]);
void populateArray(int arr[]);
void insertionsort(int arr[]);

int main(){
	int arr[MAX_SIZE];
	populateArray(arr);
	printArray(arr);
	insertionsort(arr);
	printArray(arr);
	return 0;
}

void printArray(int arr[]){
        int i;

        for(i = 0; i < MAX_SIZE; i++){
                printf("%d ", arr[i]);
        }
        printf("\n");
}

void populateArray(int arr[]){
        int i;
        srand(time(NULL));

        for(i = 0; i < MAX_SIZE; i++){
                arr[i] = rand() % 100 - 50;
        }
}

void insertionsort(int arr[]){
	int i, j, temp;

	for(i = 1; i < MAX_SIZE; i++){
		temp = arr[i];
		for(j = i - 1; j >= 0 && arr[j] > temp; j--){
			arr[j + 1] =  arr[j];
		}
		arr[j + 1] = temp;
	}
}