#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 20

void populateArray(int[]);
void printArray(int[]);
int isSorted(int[]);
void bubbieSort(int[]);

int main(){
	int numArr[SIZE];
	populateArray(numArr);
	printArray(numArr);
	printf("The numbers in the array are %sin order.\n", (isSorted(numArr) == 0)?"":"not ");
	bubbieSort(numArr);
	printArray(numArr);
	printf("The numbers in the array are %sin order.\n", (isSorted(numArr) == 0)?"":"not ");

	return 0;
}

void populateArray(int array[]){
	int i;
	srand(time(NULL));

	for(i = 0; i < SIZE; i++){
		array[i] = rand() % 100 - 50;
	}
}

void printArray(int array[]){
	int i;

	for(i = 0; i < SIZE; i++){
		printf("%d ", array[i]);
	}
	printf("\n");
}

int isSorted(int array[]){
	int i;
	for(i = 1; i < SIZE; i++){
		if(array[i] < array[i-1]){
			return 1;
		}
	}
	return 0;
}

void bubbieSort(int array[]){
	int i, j, k, temp;
	for(i = 0; i < SIZE; i++){
		k = SIZE - i;
		for(j = 1; j < k; j++){
			if(array[j] < array[j - 1]){
				temp = array[j];
				array[j] = array[j - 1];
				array[j - 1] = temp;
			}
		}
	}
}