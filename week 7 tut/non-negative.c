#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 20

void populateArray(int[]);
int nonNegative(int[]);

int main(){
	int numArr[SIZE];
	populateArray(numArr);
	printf("There are %snegative numbers in the array.\n", (nonNegative(numArr) == 0)?"no ":"");

	return 0;
}

void populateArray(int array[]){
	int i;
	srand(time(NULL));

	for(i = 0; i < SIZE; i++){
		array[i] = rand() % 100 - 50;
		printf("%d ", array[i]);
	}
}

int nonNegative(int array[]){
	int i;
	for(i = 0; i < SIZE; i++){
		if(array[i] < 0){
			return 1;
		}
	}
	return 0;
}