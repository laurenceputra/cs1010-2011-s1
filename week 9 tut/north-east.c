#include <stdio.h>

int ne(int row, int col);

int main(){
	int row, col;
	printf("Enter rows and columns apart: ");
	scanf("%d %d", &row, &col);

	printf("Number of NE-paths = %d\n", ne(row, col));
	return 0;
}

int ne(int row, int col){
	if(0 == row &&  0 == col){
		return 1;
	}
	else if(0 > row || 0 > col){
		return 0;
	}
	else {
		return ne(row - 1, col) + ne(row, col - 1);
	}
}